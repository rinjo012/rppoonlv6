﻿using System;

namespace RPPOONLV6
{
    class Program
    {
        static void Main(string[] args)
        {
            Notebook notebook = new Notebook();

            notebook.AddNote(new Note("RPPOON", "Ne zaboravi predat labose!"));
            notebook.AddNote(new Note("TI", "Predaj seminar!"));
            notebook.AddNote(new Note("Predavanje", "Petak 8:30h"));

            Iterator iterator = new Iterator(notebook);
          
            while(!iterator.IsDone)
            {
                iterator.Current.Show();
                 iterator.Next();
            }

            Box box = new Box();

            box.AddProduct(new Product("Laptop", 4799));
            box.AddProduct(new Product("Smartphone", 5999));
            box.AddProduct(new Product("Laptop", 4799));

            ProductBoxIterator PBIterator = new ProductBoxIterator(box);

            while (!PBIterator.IsDone)
            {
                string Write = PBIterator.Current.ToString();
                Console.WriteLine(Write);
                PBIterator.Next();
            }

        }
    }
}
