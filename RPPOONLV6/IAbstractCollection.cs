﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOONLV6
{
    interface IAbstractCollection
    {
        IAbstractIterator GetIterator();
    }

}
